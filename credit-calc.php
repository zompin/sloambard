<?php 
	/*
		Template name: Кредитный калькулятор
	*/
	get_header(); 
?>
<main>
	<div class="calc-page">
		<?php get_template_part('inc/credit-calc'); ?>
		<?php get_template_part('inc/cred-res'); ?>
	</div>
</main>
<?php get_footer(); ?>