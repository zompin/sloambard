<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=1200, initial-scale=1">
	<?php if (is_single()): ?>
		<meta name="description" content="<?php echo strip_tags($post->post_content); ?>">
	<?php endif;?>
	<?php wp_head(); ?>
</head>
<body>
	<header>
		<a href="<?php echo home_url(); ?>" class="logo-header"></a>
		<div class="header-contacts">
			<div class="header-contacts__item header-contacts__item_schedule">
				9.00 • 21.00
				<span>(Без выходных)</span>
			</div>
			<div class="header-contacts__item header-contacts__item_phones">
				<a href="tel:+77787400008" class="header-contacts__a">+7 (778) <span>740 00 08</span></a>
				<a href="tel:+77052999921" class="header-contacts__a">+7 (705) <span>299 99 21</span></a>
			</div>
			<a href="<?php echo home_url('/kontakty'); ?>" class="header-contacts__item header-contacts__item_lom">
				ЛОМБАРДЫ
			</a>
			<div class="header-contacts__item header-contacts__item_warn">
				<a href="tel:+77172920670" class="header-contacts__a">+7 (7172) <span>92 06 70</span></a>
			</div>
		</div>
	</header>
	<div class="header-menu-cont">
		<div class="header-menu-cont__inner">
			<ul class="main-menu header-main-menu">
				<li class="main-menu__li">
					<a href="<?php echo home_url('/o-kompanii'); ?>" class="main-menu__a">О КОМПАНИИ</a>
				</li>
				<li class="main-menu__li main-menu__li_evaluate">
					<a href="<?php echo home_url('/ocenka-onlain/'); ?>" class="main-menu__a">ОЦЕНКА ОНЛАЙН</a>
					<ul class="sub-menu">
						<li class="sub-menu__li">
							<a href="<?php echo home_url('/ocenka-uvelirnyh-izdeliy/'); ?>" class="sub-menu__a">Ювелирные изделия</a>
						</li>
						<li class="sub-menu__li">
							<a href="<?php echo home_url('/ocenka-onlain/'); ?>" class="sub-menu__a">Техника</a>
						</li>
						<li class="sub-menu__li">
							<a href="<?php echo home_url('/ocenka-onlain/'); ?>" class="sub-menu__a">Меховые изделия</a>
						</li>
					</ul>
				</li>
				<li class="main-menu__li">
					<a href="<?php echo home_url('/category/rasprodazha/'); ?>" class="main-menu__a">РАСПРОДАЖА</a>
				</li>
				<li class="main-menu__li">
					<a href="<?php echo home_url('/uslovia-zaloga'); ?>" class="main-menu__a">УСЛОВИЯ ЗАЛОГА</a>
				</li>
				<li class="main-menu__li">
					<a href="<?php echo home_url('/category/statyi/'); ?>" class="main-menu__a">СТАТЬИ</a>
				</li>
				<li class="main-menu__li">
					<a href="<?php echo home_url('/kontakty'); ?>" class="main-menu__a">КОНТАКТЫ</a>
				</li>
			</ul>
			<button class="order-call open-call-form">ЗАКАЗАТЬ ЗВОНОК</button>
			<a href="<?php echo home_url('/kreditnyy-kalkulyator/'); ?>" class="go-to-calc"></a>
			<button class="show-search"></button>
			<form action="/">
				<input type="text" class="search-field" placeholder="Введите ключевое слово" name="s">
			</form>
		</div>
	</div>
	<?php get_template_part('inc/breadcrumbs'); ?>