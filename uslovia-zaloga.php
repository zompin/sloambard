<?php 
	/*
		Template name: Условия залога
	*/
	get_header(); 
?>
<main>
	<div class="conditions">
		<div class="conditions__banner"></div>
		<h2 class="conditions__header">УСЛОВИЯ ЗАЛОГА</h2>
		<div class="conditions__items">
			<div class="conditions__item conditions__item_l">
				<p>«S-Ломбард» - лучшее место, где вы можете быстро получить деньги, заложив практически любое имущество. Вы можете сдать нам в залог:  ювелирные изделия, ноутбук, планшет, телевизор, компьютер, сотовый телефон, игровую приставку, смартфон, аудио- и видеотехнику, фотоаппараты, меховые изделия и многое другое! Мы принимаем вещи, гарантируя их бережное хранение: все вещи, заложенные в «S-Ломбард» застрахованы!</p>
			</div>
			<div class="conditions__item conditions__item_r">
				<p>Если Вы оказались в ситуации, когда вам срочно понадобились средства, вы всегда можете обратиться к нам и взять деньги под залог.</p>
				<p>Мы гарантируем полную конфиденциальность и минимальное количество документов для оформления кредита. Наш ломбард не требует различных справок и поручителей.</p>
			</div>
		</div>
		<div class="conditions__images">
			<img src="/wp-content/themes/slombard/imgs/jewels-image.png" class="conditions__image" alt="">
			<img src="/wp-content/themes/slombard/imgs/cellphones-image.png" class="conditions__image" alt="">
			<img src="/wp-content/themes/slombard/imgs/digit-image.png" class="conditions__image" alt="">
			<img src="/wp-content/themes/slombard/imgs/notebooks-image.png" class="conditions__image" alt="">
		</div>
	</div>
	<div class="how">
		<div class="how__inner">
			<div class="how__header">КАК РАБОТАЕТ «S-ЛОМБАРД»?</div>
			<div class="how__items">
				<div class="how__item how__item_l">
					<p>Клиенту для оформления займа требуется паспорт или удостоверение личности.</p>
					<p>Оформление происходит очень быстро: в течение 15 минут.</p>
					<p>Заложенную вещь может выкупить только Залогодатель, а продлить - любой человек от имени Залогодателя.</p>
					<p>Залог можно выкупить в любой день до окончания срока займа.</p>
				</div>
				<div class="how__item how__item_r">
					<p>Если Клиент выкупает залог стоимостью до 200 000 тенге раньше срока, то перерасчет не делается.</p>
					<p>Процент за пользование займом - от 0,1% в день.</p>
					<p>Размер займов - от 2000 до 10 000 000 тенге.</p>
					<p>Сроки займов - от 5 дней до 6 месяцев.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="rules">
		<div class="rules__header">ПРАВИЛА ОПЕРАЦИЙ В «S-ЛОМБАРД»</div>
		<a href="<?php echo get_post_meta(9, 'link', true); ?>" class="rules__link">ЧИТАТЬ</a>
	</div>
</main>
<?php get_footer(); ?>