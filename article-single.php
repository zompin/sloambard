<?php get_header(); ?>
<main>
	<div class="article">
		<div class="article__inner">
			<div class="article__single">
				<?php the_post(); ?>
				<h1 class="articles-page__header"><?php the_title(); ?></h1>
				<div class="articles-page__item-meta article__meta">
					<div class="articles-page__item-author">
						<?php
							$fname = get_the_author_meta('first_name');
							$sname = get_the_author_meta('last_name');
							echo $fname . ' ' . $sname;
						?>
					</div>
					<div class="articles-page__item-date"><?php the_date(); ?></div>
					<div class="articles-page__item-cat"><?php echo get_post_cat_name($post->ID, 9); ?></div>
				</div>
				<div class="article__image"><?php the_post_thumbnail(); ?></div>
				<div class="article__content content"><?php the_content(); ?></div>
				<div class="article__share">
					<span class="article__share-title">Поделиться:</span>
					<div class="pluso" data-background="transparent" data-options="small,square,line,horizontal,nocounter,theme=02" data-services="odnoklassniki,facebook,vkontakte"></div>
				</div>
				<div class="article__nav">
					<div class="article__nav-item article__nav-item_prev">
						<?php previous_post_link('%link', 'Предыдущая статья', true, 3); ?>
					</div>
					<div class="article__nav-item article__nav-item_center">
						<a href="<?php echo home_url('/category/statyi/'); ?>">Вернуться к списку статей</a>
					</div>
					<div class="article__nav-item article__nav-item_next">
						<?php next_post_link('%link', 'Следующая статья', true, 3);?>
					</div>
				</div>
			</div>
			<?php get_template_part('inc/articles-sidebar'); ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>