<?php get_header(); ?>
<main>
	<div class="content page">
		<?php get_template_part('inc/search-form'); ?>
		<?php get_template_part('inc/fail-search'); ?>
	</div>
	<?php get_template_part('inc/callback'); ?>
</main>
<?php get_footer(); ?>