<?php 
	global $wp_query;
	$query = new WP_Query(
		array_merge(
			array('posts_per_page' => 5, 'cat' => 2),
			$wp_query->query
		)
	);
?>
<div class="articles-page">
	<div class="articles-page__inner">
		<div class="articles-page__items">
			<h2 class="articles-page__header">СТАТЬИ</h2>
			<div class="articles-page__desc">Новости • Акции • Полезные советы • Юридическая информация</div>
			<?php if (have_posts()): ?>
				<?php while (have_posts()): ?>
					<?php the_post(); ?>
					<div class="articles-page__item">
						<div class="articles-page__item-header"><?php the_title(); ?></div>
						<div class="articles-page__item-meta">
							<div class="articles-page__item-author">
								<?php
									$fname = get_the_author_meta('first_name');
									$sname = get_the_author_meta('last_name');
									echo $fname . ' ' . $sname;
								?>
							</div>
							<div class="articles-page__item-date"><?php echo get_the_date(); ?></div>
							<div class="articles-page__item-cat"><?php echo get_post_cat_name($post->ID, 9); ?></div>
						</div>
						<div class="articles-page__item-image"><?php the_post_thumbnail(); ?></div>
						<div class="articles-page__item-desc content"><?php the_content(); ?></div>
						<a href="<?php the_permalink(); ?>" class="articles-page__item-link">ЧИТАТЬ</a>
					</div>
				<?php endwhile; ?>
			<?php else: ?>
				<div class="empty">Раздел пуст</div>
			<?php endif; ?>
			<?php get_template_part('inc/pagination'); ?>
		</div>
		<?php get_template_part('inc/articles-sidebar'); ?>
	</div>
	<div class="articles-page-paginator"></div>
</div>