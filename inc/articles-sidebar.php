<div class="articles-page__sidebar">
	<div class="articles-page__sidebar-search">
		<form action="/">
			<input type="text" class="articles-page__sidebar-field" name="s">
			<button class="articles-page__sidebar-button" type="submit"></button>
		</form>
	</div>
	<div class="articles-page__sidebar-nav">
		<div class="articles-page__sidebar-nav-header">КАТЕГОРИИ СТАТЕЙ</div>
		<?php wp_nav_menu(array(
			'theme_location' => 'article', 
			'container' => 'none', 
			'menu_class' => 'articles-page__sidebar-nav-menu',
			'fallback_cb' => '__return_empty_string',
			'depth' => 1
		)); ?>
	</div>
	<?php $posts = get_posts(array('cat' => 9));?>
	<?php if (count($posts)): ?>
		<div class="articles-page__sidebar-interest">
			<div class="articles-page__sidebar-interest-header">ИНТЕРЕСНЫЕ СТАТЬИ</div>
			<div class="articles-page__sidebar-interest-items">
				<?php
					foreach ($posts as $post) {
						get_template_part('inc/interest-item');
					}
				?>
			</div>
		</div>
	<?php endif; ?>
</div>