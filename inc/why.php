		<div class="why">
			<h2 class="why__header">ПОЧЕМУ КЛИЕНТЫ ВЫБИРАЮТ «S-ЛОМБАРД»?</h2>
			<div class="why__items">
				<div class="why__item why__item_issuance">Оформление <br> за 15 минут</div>
				<div class="why__item why__item_valuation">Оцениваем <br> максимально  <br> дорого!</div>
				<div class="why__item why__item_zero-percent">Займы каждому <br> новому клиенту <br> без процентов!</div>
				<div class="why__item why__item_benefit">Самые выгодные <br> проценты: <br> от 0,1% в день</div>
				<div class="why__item why__item_loans">Займы от 2000 <br> до 10 000 000 тг</div>
				<div class="why__item why__item_time">Срок от 5 <br> дней до 6 месяцев</div>
			</div>
			<button class="why__get-money open-call-form">ПОЛУЧИТЬ ДЕНЬГИ</button>
		</div>