		<div class="calc-page__inner">
			<div class="calc-page__items">
				<div class="calc-page__item">
					<h2 class="calc-page__header">РЕЗУЛЬТАТ РАСЧЕТА</h2>
					<p>Сумма к возврату:</p>
					<div class="calc-page__res">0</div>
				</div>
				<div class="calc-page__item calc-page__item_r">
					<p>Для более точного расчёта Вы можете позвонить по телефонам:</p>
					<div class="calc-page__phones">
						<a href="tel:+77787400008" class="calc-page__phone">+7 (778) 740 00 08</a>
						•
						<a href="tel:+77052999921" class="calc-page__phone">+7 (705) 299 99 21</a>
						•
						<a href="tel:+77172920670" class="calc-page__phone">+7 (7172) 92 06 70</a>
					</div>
					<p>или заказать обратный звонок.</p>
					<button class="calc-page__button open-call-form">СВЯЗАТЬСЯ С МЕНЕДЖЕРОМ</button>
				</div>
			</div>
		</div>