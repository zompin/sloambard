<div class="articles-page__sidebar-interest-item">
	<div class="articles-page__sidebar-interest-date"><?php echo get_the_date('', $post->ID); ?></div>
	<a href="<?php the_permalink($post->ID); ?>" class="articles-page__sidebar-interest-link"><?php echo $post->post_title; ?></a>
</div>