		<div class="calc">
			<div class="calc__inner">
				<input type="hidden" id='days' value="<?php echo get_post_meta(15, 'srok', true); ?>">
				<input type="hidden" id="percent" value="<?php echo get_post_meta(15, 'procent', true); ?>">
				<div class="calc__title">Кредитный <br> калькулятор</div>
				<input type="text" class="calc__sum calc__input" placeholder="<?php echo is_page_template('rate-jevels.php') ? 'Стоимость изделия' : 'Сумма'; ?>">
				<span class="calc__currency">ТЕНГЕ</span>
				<select class="calc__select calc__select_days days_select"></select>
				<span class="calc__days">ДНЕЙ</span>
				<?php if (is_home()): ?>
					<a href="<?php echo home_url('/kreditnyy-kalkulyator'); ?>" class="calc__calculate calc__calculate_save">РАССЧИТАТЬ</a>
				<?php else: ?>
					<button class="calc__calculate">РАССЧИТАТЬ</button>
				<? endif; ?>
			</div>
		</div>