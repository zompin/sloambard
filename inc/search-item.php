<div class="search-item">
	<a href="<?php the_permalink(); ?>" class="search-item__header"><?php the_title(); ?></a>
	<div class="search-item__desc"><?php echo strip_tags(get_the_content()); ?></div>
</div>