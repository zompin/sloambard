<div class="catalog__sidebar">
	<?php if (!is_single()): ?>
		<div class="filter">
			<div class="filter__title">ФИЛЬТР ПО ЦЕНЕ</div>
			<div class="filter__road"></div>
			<div class="filter__line">
				<div class="filter__currency currency">p</div>
				<div class="filter__range">0 - 500000</div>
				<button class="filter__button">ФИЛЬТР</button>
			</div>
		</div>
	<?php endif; ?>
	<div class="categories">
		<div class="categories__header">КАТЕГОРИИ ТОВАРОВ</div>
		<?php wp_nav_menu(array(
			'theme_location' => 'sale', 
			'container' => 'none', 
			'menu_class' => 'categories__ul',
			'fallback_cb' => '__return_empty_string',
			'depth' => 1
		)); ?>
	</div>
</div>