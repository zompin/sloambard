		<div class="contacts">
			<div class="contacts__contacts">
				<div class="contacts__contacts-inner">
					<div class="contacts__header">КОНТАКТЫ</div>
					<div class="contacts__central-office">ЦЕНТРАЛЬНЫЙ ОФИС</div>
					<div class="contacts__address">Астана, ул. Кенесары, 52 • ЖК «Эдем Палас 2»</div>
					<div class="contacts__phones">
						<a href="tel:+77787400008" class="contacts__phone">+7 (778) 740 00 08</a>
						<a href="tel:+77052999921" class="contacts__phone">+7 (705) 299 99 21</a>
						<a href="tel:+77172920670" class="contacts__phone">+7 (7172) 92 06 70</a>
					</div>
					<div class="contacts__schedule">9.00 - 21.00 (без выходных)</div>
					<button class="contacts__manager-call open-call-form">СВЯЗАТЬСЯ С МЕНЕДЖЕРОМ</button>
				</div>
			</div>
			<div class="contacts__map" id="map"></div>
		</div>