<div class="articles__item">
	<div class="articles__item-inner">
		<div class="articles__item-image"><?php the_post_thumbnail(); ?></div>
		<div class="articles__item-date"><?php echo get_the_date('', $post->ID); ?></div>
		<div class="articles__item-header"><?php echo $post->post_title; ?></div>
		<div class="articles__item-desc content"><?php echo $post->post_content; ?></div>
		<a href="<?php echo get_permalink($post->ID); ?>" class="articles__item-link">читать</a>
	</div>
</div>