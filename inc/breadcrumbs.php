<? if (!is_home()): ?>
	<div class="breadcrumbs">
		<div class="breadcrumbs__inner">
			<a href="<?php echo home_url('/'); ?>" class="breadcrumbs__link">Главная</a>
			<span class="breadcrumbs__back-slash">\</span>
			<?php if (is_page()): ?>
				<span class="breadcrumbs__page-name"><?php the_title(); ?></span>
			<?php elseif(is_category()): ?>
				<span class="breadcrumbs__page-name"><?php echo single_cat_title(); ?></span>
			<?php elseif (is_single()): ?>
				<?php
					$cats = wp_get_post_categories(get_the_ID());
					foreach ($cats as $cat) {
						if (cat_is_ancestor_of(2, $cat)) {
							?>
								<a href="<?php echo home_url('/category/statyi/'); ?>" class="breadcrumbs__link">Статьи</a>
								<span class="breadcrumbs__back-slash">\</span>
							<?php
							break;
						}

						if (cat_is_ancestor_of(3, $cat)) {
							?>
								<a href="<?php echo home_url('/category/rasprodazha/'); ?>" class="breadcrumbs__link">Распродажа</a>
								<span class="breadcrumbs__back-slash">\</span>
							<?php
							break;
						}
					}
				?>
				<span class="breadcrumbs__page-name"><?php the_title(); ?></span>
			<?php elseif (is_search()): ?>
				<span class="breadcrumbs__page-name">Результаты поиска</span>
			<?php elseif (is_tag()): ?>
				<span class="breadcrumbs__page-name"><?php single_tag_title('Тег: '); ?></span>
			<?php elseif (is_404()): ?>
				<span class="breadcrumbs__page-name">Ошибка 404</span>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>