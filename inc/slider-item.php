<div class="slider__item">
	<div class="slider__image" style="background-image: url(<?php echo imgsrcParse(get_the_post_thumbnail($post->ID)); ?>);"></div>
	<div class="slider__links">
		<?php $link = get_post_meta($post->ID, 'link', true); ?>
		<?php if ($link): ?>
			<a href="<?php echo $link; ?>" class="slider__link">УЗНАТЬ БОЛЬШЕ</a>
		<?php endif; ?>
		<button class="slider__link open-call-form">ПОЛУЧИТЬ</button>
	</div>
</div>