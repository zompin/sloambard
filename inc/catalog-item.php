<div class="catalog__item">
	<div class="catalog__item-inner">
		<input type="hidden" value="<?php echo get_post_meta(get_the_ID(), 'nomer', true); ?>">
		<div class="catalog__item-image">
			<?php the_post_thumbnail(); ?>
			<div class="catalog__item-buttons">
				<a href="<?php the_permalink(); ?>" class="catalog__item-button catalog__item-button_zoom"></a>
				<button class="catalog__item-button catalog__item-button_call"></button>
				<input type="hidden" value="<?php echo get_post_meta(get_the_ID(), 'nomer', true); ?>">
			</div>
		</div>
		<div class="catalog__item-cat-name"><?php echo get_post_cat_name($post->ID, 22); ?></div>
		<a href="<?php the_permalink(); ?>" class="catalog__item-name"><?php the_title(); ?></a>
		<div class="catalog__item-price">
			<span class="catalog__item-price-currency">p</span>
			<span class="catalog__item-price-value"><?php echo get_post_meta($post->ID, 'cena', true); ?></span>
		</div>
	</div>
</div>