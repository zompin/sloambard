<div class="search-form">
	<div class="search-form__header"><?php echo is_404() ? 'ОШИБКА 404' : 'Поиск'; ?></div>
	<form action="/" class="search-form__form">
		<input type="text" class="search-form__input" name="s" value="<?php echo get_search_query(); ?>">
		<button class="search-form__button" type="submit"></button>
	</form>
</div>