<?php get_header(); ?>
<main>
	<div class="page search">
		<?php get_template_part('inc/search-form'); ?>
		<?php
			if (have_posts()) {
				while (have_posts()) {
					the_post();
					get_template_part('inc/search-item');
				}
			} else {
				get_template_part('inc/fail-search');
			}
		?>
		<?php get_template_part('inc/pagination'); ?>
	</div>
	<?php get_template_part('inc/callback'); ?>
</main>
<?php get_footer(); ?>