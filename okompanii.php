<?php 
	/*
		Template name: О компании
	*/
	get_header(); 
?>
<main>
	<div class="about__banner"></div>
	<div class="about__items">
		<div class="about__item">
			<h2 class="about__header">О КОМПАНИИ</h2>
			<p class="about__p">«S-Ломбард» - молодая компания, открытая людьми много лет работающими на рынке финансовых услуг.</p>
			<p class="about__p">В «S-Ломбард» вы можете получить деньги, заложив практически любое имущество.</p>
			<div class="principles">
				<div class="principles__header">НАШИ ПРИНЦИПЫ:</div>
				<div class="principles__items">
					<div class="principles__item">Максимально качественная оценка</div>
					<div class="principles__item">Индивидуальный подход</div>
					<div class="principles__item">Низкий процент - от 0,1% в день</div>
					<div class="principles__item">Максимальная надёжность</div>
				</div>
			</div>
		</div>
		<div class="about__item in-our">
			<div class="in-our__header">В НАШЕМ ЛОМБАРДЕ ВЫ МОЖЕТЕ СДАТЬ В ЗАЛОГ:</div>
			<div class="in-our__items">
				<div class="in-our__item">ювелирные изделия</div>
				<div class="in-our__item">ЖК-телевизоры</div>
				<div class="in-our__item">сотовые телефоны</div>
				<div class="in-our__item">цифровую технику</div>
				<div class="in-our__item">компьютерную технику</div>
				<div class="in-our__item">меховые изделия</div>
			</div>
			<div class="in-our__image"></div>
		</div>
	</div>
	<div class="min-docs">
		<div class="min-docs__header">МИНИМАЛЬНОЕ КОЛИЧЕСТВО ДОКУМЕНТОВ!</div>
		<div class="min-docs__text">Мы гарантируем полную конфиденциальность и минимальное количество документов для оформления.  «S-Ломбард» не требует <br> справок и поручителей: мы выдаем деньги при наличии залоговой вещи и Вашего удостоверения личности или паспорта. <br> Проверка и оформление занимает не более 15 минут.</div>
	</div>
	<div class="refinance">
		<div class="refinance__inner">
			<div class="refinance__header">РЕФИНАНСИРОВАНИЕ</div>
			<div class="refinance__text">Рефинансирование - это выкуп Ваших вещей из других ломбардов. Если у Вас залог в ломбарде, и у Вас не хватает средств выкупить <br> его потому что, Вы испытываете определенные трудности или там высокие проценты, тогда мы придем Вам на помощь. <br> Мы поможем Вам перезаложить Ваш залог в наш ломбард. Позвоните нам, и мы подробно обо всем расскажем.</div>
		</div>
	</div>
	<div class="support">
		<div class="support__text">МЫ ВСЕГДА ПОСТАРАЕМСЯ ВАС ПОДДЕРЖАТЬ В НУЖНЫЙ МОМЕНТ!</div>
		<button class="support__button open-call-form">БЫСТРАЯ ПОМОЩЬ</button>
	</div>
	<?php get_template_part('inc/why'); ?>
	<?php get_template_part('inc/contacts'); ?>
</main>
<?php get_footer(); ?>