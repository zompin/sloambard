(function($) {
	$(document).ready(function() {
		
		$('.show-search').click(function() {
			$('.search-field').toggle().focus();
		});

		$('.slider__items').slick({
			arrows: false,
			dots: true,
			appendDots: '.slider__dots',
			autoplay: true,
			fade: true,
			speed: 2000
		});

		$('.articles__items').slick({
			arrows: false,
			dots: true,
			appendDots: '.articles__dots',
			slidesToShow: 3,
			slidesToScroll: 1
		});

		$('.days_select').append(function() {
			var days = $('#days').val();
			var options = '<option value="0" celected="celected">Выберите срок</option>';

			if (days < 1) {
				days = 1;
			}

			for (var i = 1; i <= days; i++) {
				options += '<option value="' + i + '">' + i + '</option>'
			}

			return options;

		});

		$('.open-call-form').click(function() {
			makeTimeSelect();
			$('.call').css('display', 'flex');
		});

		function makeTimeSelect() {
				$('.time-select').append(function() {
				var options = '<option value="0"></option>';

				for (var i = 1; i <= 24; i++) {
					options += '<option value="' + i + '.00">' + i + '.00</option>';
				}

				return options;
			});
		}

		$('.call__close, .call__close-div').click(function() {
			$('.call').hide();
			$('.call__textarea').hide();
			$('.call__input_name').val('');
			$('.call__input_phone').val('');
			$('.call__select_from').val('');
			$('.call__select_to').val('');
			$('.call__textarea').val('');
		});

		$('.jevels-rate_q').mouseenter(function() {
			$('.jevels-rate_q-text').stop().fadeIn();
		});

		$('.jevels-rate_q').mouseleave(function() {
			$('.jevels-rate_q-text').stop().fadeOut();
		});

		$('.calc__sum').keyup(function() {
			calculateCredit();
		});

		$('.calc__select_days').change(function() {
			calculateCredit();
		});

		$('.calc__calculate').click(function() {
			calculateCredit();
		});

		$('.calc__calculate_save').click(function() {
			var calc = {};

			calc.sum = $('.calc__sum').val() * 1;
			calc.srok = $('.calc__select_days').val() * 1;

			if (calc.sum && calc.srok) {
				$.cookie('calc', JSON.stringify(calc), {path: '/'});
			}
		});

		if (!$('#gold').length) {
			var calc = $.cookie('calc');

			if (calc) {

				calc = JSON.parse(calc);

				if (calc) {
					$('.calc__sum').val(calc.sum);
					$('.calc__select_days').val(calc.srok);
					calculateCredit();
				}
			}
		}

		function calculateCredit() {
			var sum = $('.calc__sum').val() * 1;
			var days = $('.calc__select_days').val() * 1;
			var percent = $('#percent').val() * 1;

			sum += (sum / 100) * percent * days;

			if (sum != sum || days == 0) {
				sum = 0;
			}
			$('.calc-page__res').html(sum.toFixed(2));

			if ($('.calc-page__res').length) {
				$.removeCookie('calc', {'path': '/'});
			}
		}

		$('.jevels-rate__mode label').click(function() {
			var mode = $('input[name=jevels-mode]:checked').val();
			setMetalSelect(mode);
			calculateMetal();
		});

		$('.calc__input_weight').keyup(function() {
			calculateMetal();
		});

		$('.select_proba').change(function() {
			calculateMetal();
		});

		$('.jevels-rate__lom').change(function() {
			calculateMetal();
		});

		$('.jevels-rate__button').click(function() {
			calculateMetal();
		});

		setMetalSelect();
		calculateMetal();

		function calculateMetal() {
			var weight = $('.calc__input_weight').val() * 1;
			var lom = $('.jevels-rate__lom').prop('checked');
			var priceForGramm = $('.select_proba').val() * 1;
			var skidkaLom = $('#lom').val() * 1;
			var mode = $('input[name=jevels-mode]:checked').val();

			if (!$('#gold').length) {
				return;
			}


			var sum = weight * priceForGramm;
			
			if (lom) {
				sum -= (sum / 100) * skidkaLom;
			}

			if (sum != sum) {
				sum = 0;
			}

			$('.calc__sum').val(sum).prop('disabled', true);
			calculateCredit();
		}

		function setMetalSelect(mode) {
			var raw;
			var options = '<option value="0">Выберите пробу</option>';

			if (!$('#gold').length) {
				return;
			}

			if (mode == 'silver') {
				elem = 'silver';
			} else {
				elem = 'gold';
			}

			raw = $('#' + elem).val().replace(/,/g, '.').split('&');

			for (var i = 0; i < raw.length; i++) {
				raw[i] = raw[i].split('-');
				options += '<option value="' + raw[i][1] + '">' + raw[i][0] + '</option>';
			}

			$('.select_proba').html(options);
		}

		var callTimeout = animateButton();

		function animateButton() {
			return setInterval(function() {
				$('.float-button')
				.animate({
					top: '-=100'
				}, 200)
				.animate({
					top: '+=100'
				}, 200)
				.animate({
					top: '-=50'
				}, 100)
				.animate({
					top: '+=50'
				}, 100)
				.animate({
					top: '-=25'
				}, 50)
				.animate({
					top: '+=25'
				}, 50);
			}, 10000);
		}

		$('.float-button').mouseenter(function() {
			clearInterval(callTimeout);
		});

		$('.float-button').mouseleave(function() {
			callTimeout = animateButton();
		});

		var floatButton = $('.float-button__cont');
		floatButton.__position = 'absolute';
		$(document).scroll(function() {
			var offset = $(document).scrollTop();
			if (floatButton.__position == 'absolute') {
				console.log(11)
				if (offset >= 500) {
					floatButton.__position = 'fixed';
					floatButton.css('position', 'fixed');
					floatButton.css('top', 'auto');
					floatButton.css('bottom', '100px');
				}
			} else {
				if (offset < 500) {
					floatButton.__position = 'absolute';
					floatButton.css('position', 'absolute');
					floatButton.css('top', '750px');
					floatButton.css('bottom', 'auto');
				}
			}
		});

		$('.call__input_phone').mask('+7 (999) 999-99-99');

		$('.catalog__button_cheap').click(function() {
			$('#order').val('DESC');
		});

		$('.rate-form__button').click(function() {
			var cat = $('.rate-form__select_cat').val();
			var goods = $('.rate-form__input_goods').val();
			var photo = $('.rate-form__file_photo').prop('files')[0];
			var desc = $('.rate-form__textarea_desc').val();
			var rate = $('input[name=rate]:checked').val();
			var toClient = $('input[name=to-client]:checked').val();
			var name = $('.rate-form__input_name').val();
			var phone = $('.rate-form__input_phone').val();
			var sms = $('.rate-form__checkbox_sms').prop('checked');
			var email = $('.rate-form__input_email').val();
			var mails = $('.rate-form__checkbox_mails').prop('checked');
			var captcha = $('.rate-form__input_captcha').val();
			var fd = new FormData();

			if (!cat || !goods || !photo || !name || !phone || !captcha) {
				alert('Заполните обязательные поля');
				return;
			}

			fd.append('photo', photo);
			fd.append('cat', cat);
			fd.append('goods', goods);
			fd.append('desc', desc);
			fd.append('rate', rate);
			fd.append('toClient', toClient);
			fd.append('name', name);
			fd.append('phone', phone);
			fd.append('sms', sms);
			fd.append('email', email);
			fd.append('mails', mails);
			fd.append('captcha', captcha);

			$.ajax({
				url: '/wp-content/themes/slombard/rate.php',
				type: 'POST',
				data: fd,
				processData: false,
				contentType: false,
				success: function(data) {
					if (data == 1) {
						$('.alert').css('display', 'flex');
						$('.rate-form__select_cat').val('');
						$('.rate-form__input_goods').val('');
						$('.rate-form__file_photo').val('');
						$('.rate-form__textarea_desc').val('');
						$('input[name=rate]:checked').val('Новый');
						$('input[name=to-client]:checked').val('Нет');
						$('.rate-form__input_name').val('');
						$('.rate-form__input_phone').val('');
						$('.rate-form__checkbox_sms').prop('checked', false);
						$('.rate-form__input_email').val('');
						$('.rate-form__checkbox_mails').prop('checked', false);
						$('.rate-form__input_captcha').val('');
						$('.rrate-form_file-name').empty();
					} else if (data == 2) {
						alert('Каптча введена неверно');
					} else {
						alert('Ошибка');
					}
				}
			});
		});

		$('.alert__close, .alert__close-div').click(function() {
			$('.alert').hide();
		});

		$('.rate-form__file_photo').change(function() {
			$('.rate-form_file-name').html($(this).val());
		});

		$('.call__button').click(function() {
			var data = {};
			data.name = $('.call__input_name').val();
			data.phone = $('.call__input_phone').val();
			data.from = $('.call__select_from').val();
			data.to = $('.call__select_to').val();
			data.goods = $('.call__textarea').val();
			data.nomer = $('.call').attr('data-nomer');

			if (!data.name || !data.phone) {
				alert('Введите имя и телефон');
				return;
			}

			$.ajax({
				url: '/wp-content/themes/slombard/order-call.php',
				type: 'POST',
				data: data,
				success: function(data) {
					if (data == 1) {
						$('.alert').css('display', 'flex');
						$('.call').hide();
						$('.call__input_name').val('');
						$('.call__input_phone').val('');
						$('.call__select_from').val('');
						$('.call__select_to').val('');
					} else {
						alert('Ошибка');
					}
				}
			});
		});

		$('.catalog__item-button_call').click(function() {
			var desc = $(this).parent().parent().next().next().text();
			makeTimeSelect();
			$('.call').css('display', 'flex');
			$('.call__textarea').show().val(desc);
		});

		$('.catalog__order').click(function() {
			var desc = $(this).parent().find('h1').text();
			var nomer = $(this).next().val();
			makeTimeSelect();
			$('.call').css('display', 'flex');
			$('.call__textarea').show().val(desc);
			$('.call').attr('data-nomer', nomer);
		});

		$('.image__close, .image__close-div').click(function() {
			$('.image').hide();
			$('.image__items').slick('unslick').empty();
		});


		$('.catalog__main-image').click(function() {
			var img = $(this).parent().parent().find('img').clone();
			var src = $(img[0]).attr('src');
			var newImg = new Image();
			newImg.src = src;

			newImg.onload = function() {
				var width = newImg.width;
				$('.image__items').html(img).width(width);
				$('.image').css('display', 'flex');
				$('.image__items').slick({
					nextArrow: '.image__button_next',
					prevArrow: '.image__button_prev'
				});
			}
		});

		$('.catalog__optional-image').mouseenter(function() {
			var src = $(this).find('img').attr('src');
			$('.catalog__optional-image').removeClass('catalog__optional-image_current');
			$(this).addClass('catalog__optional-image_current');
			$('.catalog__main-image').find('img').attr('src', src);
		});

		$('.catalog__optional-images').mouseleave(function() {
			var src = $('.catalog__optional-image').find('img').eq(0).attr('src');
			$('.catalog__optional-image').removeClass('catalog__optional-image_current');
			$('.catalog__optional-image').eq(0).addClass('catalog__optional-image_current');
			$('.catalog__main-image').find('img').attr('src', src);
		});

		$('.catalog__search-button').click(function() {
			var searchVal = $('.catalog__search-field').val();
			$.cookie('search', searchVal);
		});

		if ($.cookie('search')) {
			$('.catalog__search-field').val($.cookie('search'));
		}

		$('.catalog__button_cheap').click(function() {
			var href = '/category/rasprodazha/?order=ASC';
			if ($('.catalog__search-field').val()) {
				href += '&keyword=' + $('.catalog__search-field').val();
			}
			location.href = href;
		});

		$('.catalog__button_expensive').click(function() {
			var href = '/category/rasprodazha/?order=DESC';
			if ($('.catalog__search-field').val()) {
				href += '&keyword=' + $('.catalog__search-field').val();
			}
			location.href = href;
		});

		$('.catalog__button_new').click(function() {
			var href = '/category/rasprodazha/';
			if ($('.catalog__search-field').val()) {
				href += '?keyword=' + $('.catalog__search-field').val();
			}
			location.href = href;
		});

		$('.filter__road').slider({
			range: true,
			min: 0,
			max: 1000000,
			values: [0, 500000],
			classes: {
				"ui-slider": "filter__road",
				"ui-slider-handle": "filter__seek",
				"ui-slider-range": "filter__space"
			},
			slide: function(e, ui) {
				var values = ui.values;
				$('.filter__range').html(values[0] + ' - ' + values[1]);
				//$('.hidden').html('<input type="hidden" name="price_from" value="' + values[0] + '"><input type="hidden" name="price_to" value="' + values[1] + '">');
			}
		});

		$('.filter__button').click(function() {
			var values = $('.filter__range').text().split(' - ');
			$('.hidden').html('<input type="hidden" name="price_from" value="' + values[0] + '"><input type="hidden" name="price_to" value="' + values[1] + '">');
			$('.catalog__search-button').click();
		});
	});


})(jQuery);