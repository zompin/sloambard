ymaps.ready(function() {
	var mapE = document.getElementById('map');
	if (!mapE) {
		return;
	}
	
	var map = new ymaps.Map(mapE, {
		center: [51.169861, 71.439112], 
		zoom: 14,
		controls: []
	});

	var template = ymaps.templateLayoutFactory.createClass('<div class=\"map-mark\"></div>');
	var placeMark1 = new ymaps.Placemark(
			[51.166923, 71.441428], {}, {
				iconLayout: template
			}
		);
	var placeMark2 = new ymaps.Placemark(
			[51.173477, 71.438468], {}, {
				iconLayout: template
			}
		);
	map.geoObjects.add(placeMark1);
	map.geoObjects.add(placeMark2);
});