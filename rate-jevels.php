<?php 
	/*
		Template name: Оценка ювелирных изделий
	*/
	get_header(); 
?>
<main>
	<div class="jevels-rate">
		<div class="jevels-rate__inner">
			<input type="hidden" value="<?php echo implode('&', get_post_meta(13, 'proba_z')); ?>" id="gold">
			<input type="hidden" value="<?php echo implode('&', get_post_meta(13, 'proba_s')); ?>" id="silver">
			<input type="hidden" value="<?php echo implode('&', get_post_meta(13, 'skidka_lom')); ?>" id="lom">
			<span class="jevels-rate__mode">
				<label>
					<input type="radio" name="jevels-mode" checked="checked" value="gold">
					<span>ЗОЛОТО</span>
				</label>
				<br>
				<label>
					<input type="radio" name="jevels-mode" value="silver">
					<span>СЕРЕБРО</span>
				</label>
			</span>
			<input type="text" class="calc__input calc__input_weight jevels-rate__input" placeholder="Укажите примерны вес">
			<span class="jevels-rate__weight-title">ГРАММ</span>
			<select class="calc__select select_proba">
			</select>
			<span class="jevels-rate__quality">
				<label>
					<input type="checkbox" class="jevels-rate__lom">
					ЛОМ
				</label>
				<span class="jevels-rate_q">
					<span>
						<span class="jevels-rate_q-text">Если ваше изделие повреждено, не имеет четко читаемой пробы, не имеет пары в случае парных изделий, то его следует рассматривать как лом.</span>
					</span>
				</span>
			</span>
			<button class="jevels-rate__button">УЗНАТЬ СТОИМОСТЬ</button>
		</div>
		<?php get_template_part('inc/credit-calc'); ?>
		<?php get_template_part('inc/cred-res'); ?>
	</div>
</main>
<?php get_footer(); ?>