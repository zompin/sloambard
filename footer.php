	<footer>
		<div class="footer">
			<div class="footer__menu-container">
				<a href="<?php echo home_url(); ?>" class="footer__logo"></a>
				<ul class="footer__main-menu main-menu">
					<li class="main-menu__li">
						<a href="<?php echo home_url('/o-kompanii'); ?>" class="main-menu__a">О КОМПАНИИ</a>
					</li>
					<li class="main-menu__li">
						<a href="<?php echo home_url('/ocenka-onlain/'); ?>" class="main-menu__a">ОЦЕНКА ОНЛАЙН</a>
					</li>
					<li class="main-menu__li">
						<a href="<?php echo home_url('/category/rasprodazha/'); ?>" class="main-menu__a">РАСПРОДАЖА</a>
					</li>
					<li class="main-menu__li">
						<a href="<?php echo home_url('/uslovia-zaloga'); ?>" class="main-menu__a">УСЛОВИЯ ЗАЛОГА</a>
					</li>
					<li class="main-menu__li">
						<a href="<?php echo home_url('/category/statyi/'); ?>" class="main-menu__a">СТАТЬИ</a>
					</li>
					<li class="main-menu__li">
						<a href="<?php echo home_url('/kontakty'); ?>" class="main-menu__a">КОНТАКТЫ</a>
					</li>
				</ul>
			</div>
			<div class="year">© 2016</div>
			<div class="copy">Товарищество с ограниченной ответственностью «Kiwi Gold»</div>
			<nav class="social-menu">
				<a href="" class="social-menu__item social-menu__item_ok"></a>
				<a href="" class="social-menu__item social-menu__item_vk"></a>
				<a href="" class="social-menu__item social-menu__item_inst"></a>
			</nav>
		</div>
	</footer>
	<div class="float-button__cont">
		<button class="float-button open-call-form"></button>
	</div>
	<div class="alert">
		<div class="alert__close-div"></div>
		<div class="alert__box">
			<button class="alert__close"></button>
			<div class="alert__header">СПАСИБО!</div>
			<p>Ваш запрос принят.</p>
			<p>Мы обязательно ответим Вам <br> в ближайшее время.</p>
		</div>
	</div>
	<div class="call">
		<div class="call__close-div"></div>
		<div class="call__inner">
			<button class="call__close"></button>
			<div class="call__header">Здравствуйте!</div>
			<div class="call__text">Оставьте номер, и мы Вам перезвоним!</div>
			<div class="call__line">
				<span class="call__pre-input">Имя*</span>
				<input type="text" class="call__input call__input_name">
			</div>
			<div class="call__line">
				<span class="call__pre-input">Телефон*</span>
				<input type="text" class="call__input call__input_phone" placeholder="+7 (___) ___-__-__">
			</div>
			<div class="call__line call__line_schedule">
				<div class="call__text">Удобное Вам время звонка:</div>
				<span class="call__pre-select">C</span>
				<select class="call__select call__select_from time-select"></select>
				<span class="call__pre-select call__pre-select_before">До</span>
				<select class="call__select call__select_to time-select"></select>
			</div>
			<div class="call__line">
				<textarea class="call__textarea"></textarea>
			</div>
			<button class="call__button">ОСТАВИТЬ ЗАЯВКУ</button>
		</div>
	</div>
	<div class="image">
		<div class="image__close-div"></div>
		<div class="image__inner">
			<button class="image__close"></button>
			<div class="image__items"></div>
			<div class="image__nav">
				<button class="image__button image__button_prev"></button>
				<button class="image__button image__button_next"></button>
			</div>
		</div>
	</div>
	<?php wp_footer(); ?>
</body>
</html>