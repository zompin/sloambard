<?php get_header(); ?>
<main>
	<?php
		$cats = wp_get_post_categories(get_the_ID());
		foreach ($cats as $cat) {
			if (cat_is_ancestor_of(2, $cat)) {
				get_template_part('article-single');
				break;
			}

			if (cat_is_ancestor_of(3, $cat)) {
				get_template_part('catalog-single');
				break;
			}
		}
	?>
</main>
<?php get_footer(); ?>