<?php get_header(); ?>
<main>
	<?php
		$current_cat = get_query_var('cat');

		if (cat_is_ancestor_of(3, $current_cat) || $current_cat == 3) {
			get_template_part('catalog-category');
		} elseif (cat_is_ancestor_of(2, $current_cat) || $current_cat == 2) {
			get_template_part('articles-category');
		}
	?>
</main>
<?php get_footer(); ?>