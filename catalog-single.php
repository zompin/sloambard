	<div class="catalog">
		<?php the_post(); ?>
		<div class="catalog__inner">
			<div class="catalog__images">
				<div class="catalog__main-image"><?php the_post_thumbnail(); ?></div>
				<div class="catalog__optional-images">
				<?php
					$imgs = imgsParse(get_the_content());
					$imgStr = '<div class="catalog__optional-image catalog__optional-image_current">' . get_the_post_thumbnail() . '</div>';

					foreach ($imgs as $img) {
						$imgStr .= '<div class="catalog__optional-image">' . $img . '</div>';
					}

					echo $imgStr;
				?>
				</div>
			</div>
			<div class="catalog__info">
				<h1 class="catalog__header"><?php the_title(); ?></h1>
				<div class="catalog__category">Цифровая техника</div>
				<div class="catalog__price">
					<span class="catalog__price-currency">p</span>
					<span class="catalog__price-value"><?php $price = get_post_meta(get_the_ID(), 'cena', true); echo $price; ?></span>
				</div>
				<div class="catalog__desc content"><?php the_content();?></div>
				<div class="catalog__position">
					<span class="catalog__position-title">Местонахождение:</span>
					<span class="catalog__position-value"><?php echo get_post_meta(get_the_ID(), 'mesto', true); ?></span>
				</div>
				<div class="catalog__date">
					<span class="catalog__date-title">Дата поступления:</span>
					<span class="catalog__date-value"><?php echo get_the_date(); ?></span>
				</div>
				<div class="catalog__tags">
					<span class="catalog__tags-title">Теги:</span>
					<span class="catalog__tags-value"><?php the_tags(''); ?></span>
				</div>
				<button class="catalog__order">ОСТАВИТЬ ЗАЯВКУ</button>
				<input type="hidden" value="<?php echo get_post_meta(get_the_ID(), 'nomer', true); ?>">
			</div>
		</div>
		<?php get_template_part('inc/catalog-sidebar'); ?>
	</div>
	<?php
		$posts = get_posts(array(
			'cat' => 3,
			'numberposts' => 4,
			'key' => 'cena',
			'value' => array((int) ($price - 10000), (int) ($price + 1000)),
			'type' => 'numeric',
			'compare' => 'between',
			'exclude' => get_the_ID()
		));
	?>
	<?php if (count($posts)): ?>
		<div class="similar">
			<div class="similar__inner">
				<div class="similar__header">ПОХОЖИЕ ТОВАРЫ:</div>
				<div class="similar__items">
					<?php
						$posts = get_posts(array(
								'cat' => 3,
								'numberposts' => 4,
								'key' => 'cena',
								'value' => array((int) ($price - 10000), (int) ($price + 1000)),
								'type' => 'numeric',
								'compare' => 'between',
								'exclude' => get_the_ID()
							));
						foreach ($posts as $post) {
							get_template_part('inc/catalog-item');
							get_template_part('inc/catalog-item');
						}
					?>
				</div>
			</div>
		</div>
	<?php endif;?>