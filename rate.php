<?php
	session_start();
	require 'mailer/PHPMailerAutoload.php';
	require 'mailer/config.php';

	if (md5($_POST['captcha']) != $_SESSION['randomnr2']) {
		echo 2;
		exit;
	}

	$html = getFile();
	
	$html .= '
		<tr>
			<td>Имя:</td>
			<td>' . $_POST['name'] . '</td>
		</tr>
		';
	$html .= '
		<tr>
			<td>Телефон:</td>
			<td>' . $_POST['phone'] . '</td>
		</tr>
		';
	
	if ($_POST['email']) {
		$html .= '
			<tr>
				<td></td>
				<td></td>
			</tr>
			';
	}

	$html .= '
		<tr>
			<td>Наименование товара:</td>
			<td>' . $_POST['goods'] . '</td>
		</tr>
		';

	$html .= '
		<tr>
			<td>Категория:</td>
			<td>' . $_POST['cat'] . '</td>
		</tr>
		';

	if ($_POST['desc']) {
		$html .= '
			<tr>
				<td>Описание:</td>
				<td>' . $_POST['desc'] . '</td>
			</tr>
			';
	}

	$html .= '
		<tr>
			<td>Состояние:</td>
			<td>' . $_POST['rate'] . '</td>
		</tr>
		';

	$html .= '
		<tr>
			<td>Выезд к клиенту:</td>
			<td>' . $_POST['toClient'] . '</td>
		</tr>
		';

	if ($_POST['sms']) {
		$html .= '
			<tr>
				<td colspan="2">Я согласен(а) получать СМС-уведомления о выгодных преlложениях от S-Ломбард</td>
			</tr>
			';
	}

	if ($_POST['mails']) {
		$html .= '
			<tr>
				<td colspan="2">Я согласен(а) получать уведомления о выгодных преlложениях от S-Ломбард</td>
			</tr>
			';
	}

	$html = '<table>' . $html . '</table>';
	$mail->Body = $html;
	
	if ($mail->send()) {
		echo 1;
	} else {
		echo 0;
	}

	function getFile() {
		$html = '';

		if (count($_FILES)) {
			$file = $_FILES['photo'];
			if ($file && $file['error'] === 0) {
				$name = $file['name'];
				$tmpName = $file['tmp_name'];
				$type = $file['type'];
				$fp = fopen($tmpName, "r");
				$fcontent = base64_encode(fread($fp, filesize($tmpName)));
				$imageContent = "<img src=\"data:" . $type . ";base64," . $fcontent . "\" style='width: 100%; height: auto;'>";
				$html .= '<tr><td colspan="2">' . $imageContent . '</td></tr>';
				fclose($fp);
			}
		}

		return $html;
	}
?>