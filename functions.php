<?php

function slombard_setup() {
	
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	register_nav_menus( array(
		'sale' => 'Меню распродажи',
		'article'  => 'Меню статей',
	) );
}
add_action( 'after_setup_theme', 'slombard_setup' );

function slombard_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_script( 'jq', get_template_directory_uri() . '/js/libs/jquery/dist/jquery.min.js', null, null, true );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/libs/slick-carousel/slick/slick.min.js', null, null, true );
	wp_enqueue_script( 'jcookie', get_template_directory_uri() . '/js/libs/jquery.cookie/jquery.cookie.js', null, null, true );
	wp_enqueue_script( 'jui', get_template_directory_uri() . '/js/libs/jquery-ui/jquery-ui.min.js', null, null, true );
	wp_enqueue_script( 'mask', get_template_directory_uri() . '/js/maskinput.js', null, null, true );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', null, null, true );
	wp_enqueue_script( 'ymapi', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU', null, null, true );
	wp_enqueue_script( 'map', get_template_directory_uri() . '/js/map.js', null, null, true );
	wp_enqueue_script( 'social', get_template_directory_uri() . '/js/social.js', null, null, true );
}
add_action( 'wp_enqueue_scripts', 'slombard_scripts' );

function get_post_cat_name($post_id, $ignore_cat_id) {
	$cats = wp_get_post_categories($post_id);
	$catNames = array();

	foreach ($cats as $k => $v) {
		$cats[$k] = get_category($v);
	}

	foreach ($cats as $cat) {

		if ($cat->cat_ID == 1 || $cat->cat_ID == 2 || $cat->cat_ID == 3) {
			continue;
		}

		if ($cat->cat_ID == $ignore_cat_id) {
			if (count($cats) == 1) {
				array_push($catNames, $ignore_cat_id);
			} else {
				continue;
			}
		}

		array_push($catNames, $cat->cat_name);
	}

	return implode($catNames, ', ');
}

function imgsrcParse($str, $pattern = '/src="([^"]*)"/isU') {
	$pieces;
	$res = preg_match($pattern, $str, $pieces);

	if ($res) {
		$res = $pieces[1];
	} else {
		$res = !!$res;
	}

	return $res;
}

function imgsParse($str) {
	$img_no_tags = array();
	preg_match_all('/<img[^>]+>/i',$str, $result);

	return $result[0];
}

function catalogFilter() {
	global $wp_query;
	$args = array('cat' => 3);

	if ($_GET['keyword'] != '') {
		$args['s'] = $_GET['keyword'];
	}

	if ($_GET['price_from'] != '' && $_GET['price_to'] != '') {
		$args['meta_query'][] = array(
				'key' => 'cena',
				'value' => array((int) $_GET['price_from'], (int) $_GET['price_to']),
				'type' => 'numeric',
				'compare' => 'between'
			);
	}

	if ($_GET['order'] != '') {
		if ($_GET['order'] == 'ASC') {
			$args['order'] = 'ASC';	
		} else {
			$args['order'] = 'DESC';
		}
		$args['meta_query'] = array('relation' => 'AND');
		$args['orderby'] = 'meta_value_num';
		$args['meta_query'][] = array(
				'key' => 'cena',
				'type' => 'NUMERIC',
			);
	}

	query_posts(array_merge($wp_query->query, $args));
}

function myNavigationTemplate($template, $class) {
	return '
		<nav class="navigation %1$s" role="navigation">
			<div class="nav-links">%3$s</div>
		</nav>
	';
}
add_filter('navigation_markup_template', 'myNavigationTemplate', 10, 2 );

function remove_img_attr ($html) {
    return preg_replace('/(width|height|srcset)="\d+"\s/', "", $html);
}
add_filter('post_thumbnail_html', 'remove_img_attr');
//add_filter('the_content', 'remove_img_attr');
//add_filter('image_send_to_editor', 'remove_img_attr');

function disable_srcset( $sources ) {
	return false;
}
add_filter( 'wp_calculate_image_srcset', 'disable_srcset' );

function searchFilter($query) {
	if ($query->is_search) {
		$query->set('cat', '-1,-17');
	}

	return $query;
}
add_filter('pre_get_posts', 'searchFilter');