<?php 
	global $wp_query;
	$query = new WP_Query(
		array_merge(
			array('posts_per_page' => 15, 'cat' => 3),
			$wp_query->query
		)
	);
?>
<div class="catalog">
	<div class="catalog__items">
		<?php get_template_part('inc/catalog-search'); ?>
		<div class="catalog__items-header">
			<span class="catalog__sort-by">УПОРЯДОЧИТЬ ПО:</span>
			<button class="catalog__button catalog__button_cheap">Сначала дешевые</button>
			<button class="catalog__button catalog__button_expensive">Сначала дорогие</button>
			<button class="catalog__button catalog__button_new">Сначала новые</button>
		</div>
		<div class="catalog__items-inner">
			<?php
				if ($_GET && !empty($_GET)) {
					catalogFilter();
				}
			?>
			<?php if (have_posts()): ?>
				<?php while (have_posts()): ?>
					<?php the_post(); ?>
					<?php get_template_part('inc/catalog-item'); ?>
				<?php endwhile; ?>
			<?php else: ?>
				<div class="empty">Раздел пуст</div>
			<?php endif; ?>
		</div>
		<?php get_template_part('inc/pagination'); ?>
	</div>
	<?php get_template_part('inc/catalog-sidebar'); ?>
</div>