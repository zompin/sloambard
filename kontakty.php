<?php 
	/*
		Template name: Контакты
	*/
	get_header(); 
?>
<main>
	<div class="contacts-page">
		<div class="contacts-page__inner">
			<div class="contacts-page__map" id="map"></div>
			<h2 class="contacts-page__header">КОНТАКТЫ</h2>
			<div class="contacts-page__items">
				<div class="contacts-page__item">
					<div class="contacts-page__item-header">ЦЕНТРАЛЬНЫЙ ОФИС</div>
					<div class="contacts-page__address">Астана, ул. Кенесары, 52 • ЖК «Эдем Палас 2»</div>
					<div class="contacts-page__phones">
						<a href="tel:+77787400008" class="contacts-page__phone">+7 (778) 740 00 08</a>
						•
						<a href="tel:+77052999921" class="contacts-page__phone">7 (705) 299 99 21</a>
						•
						<a href="tel:+77172920670" class="contacts-page__phone">+7 (7172) 92 06 70</a>
					</div>
					<div class="contacts-page__schedule">9.00 - 21.00 (без выходных)</div>
				</div>
				<div class="contacts-page__item">
					<div class="contacts-page__item-header">ФИЛИАЛ №2</div>
					<div class="contacts-page__address">Астана, ул Валиханова № 22a, ТД «Каракоз» (напротив ТД «Артем»)</div>
					<div class="contacts-page__phones">
						<a href="tel:+77787111151" class="contacts-page__phone">+7 (778) 711 11 51</a>
						•
						<a href="tel:+77771333329" class="contacts-page__phone">+7 (777) 133 33 29</a>
						•
						<a href="tel:+77172920670" class="contacts-page__phone">+7 (7172) 92 06 70</a>
					</div>
					<div class="contacts-page__schedule">9.00 - 21.00 (без выходных)</div>
				</div>
			</div>
			<div class="contacts-page__control">
				<div class="contacts-page__item-header">КОНТРОЛЬ КАЧЕСТВА УСЛУГ</div>
				<a href="tel:+77172920670" class="contacts-page__phone contacts-page__phones">+7 (7172) 92 06 70</a>
			</div>
		</div>
		<?php get_template_part('inc/callback'); ?>
	</div>
</main>
<?php get_footer(); ?>