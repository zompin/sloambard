<?php get_header(); ?>
	<main>
		<?php $posts = get_posts(array('cat' => 17, 'numberposts' => -1)); ?>
		<?php if (count($posts)): ?>
			<div class="slider">
				<div class="slider__items">
					<?php
						foreach ($posts as $post) {
							get_template_part('inc/slider-item');
						}
					?>
				</div>
				<div class="slider__dots"></div>
			</div>
		<?php endif;?>
		<?php get_template_part('inc/credit-calc'); ?>
		<div class="sale">
			<h2 class="sale__header">РАСПРОДАЖА</h2>
			<div class="sale__items">
				<a href="<?php echo home_url('/category/rasprodazha/uvelirnye-izdeliya/'); ?>" class="sale__item sale__item_jevels">Ювелирные <br> изделия</a>
				<a href="<?php echo home_url('/category/rasprodazha/sotovye-telefony/'); ?>" class="sale__item sale__item_cellphones">Сотовые <br>телефоны</a>
				<a href="<?php echo home_url('/category/rasprodazha/noutbuki/'); ?>" class="sale__item sale__item_notebooks">Ноутбуки</a>
				<a href="<?php echo home_url('/category/rasprodazha/zhk-televizory/'); ?>" class="sale__item sale__item_tv">ЖК-телевизоры</a>
				<a href="<?php echo home_url('/category/rasprodazha/cyfrovaya-tehnika/'); ?>" class="sale__item sale__item_digit">Цифровая <br> техника</a>
				<a href="<?php echo home_url('/category/rasprodazha/mehovye-izdeliya/'); ?>" class="sale__item sale__item_fur">Меховые <br> изделия</a>
			</div>
		</div>
		<?php 
			$posts = get_posts(array(
					'category' => 22
				));
			if (count($posts)): 
		?>
			<div class="catalog">
				<div class="catalog__items">
					<?php get_template_part('inc/catalog-search'); ?>
					<div class="catalog__items-header">ИЗБРАННЫЕ ТОВАРЫ</div>
					<div class="catalog__items-inner">
						<?php
							foreach ($posts as $post) {
								get_template_part('inc/catalog-item');
							}
						?>
					</div>
					<a href="<?php echo home_url('/category/rasprodazha/'); ?>" class="catalog__go-to-catalog">ПЕРЕЙТИ В КАТАЛОГ</a>
				</div>
				<?php get_template_part('inc/catalog-sidebar'); ?>
			</div>
		<?php endif;?>

		<?php get_template_part('inc/why'); ?>

		<?php
			$posts = get_posts(array('cat' => 2));
		?>
		<?php if (count($posts)): ?>
			<div class="articles">
				<h2 class="articles__header">СТАТЬИ</h2>
				<div class="articles__items">
					<?php 
						foreach ($posts as $post) {
							get_template_part('inc/article-item');
						}
					?>
				</div>
				<div class="articles__dots"></div>
			</div>
		<?php endif; ?>

		<?php get_template_part('inc/contacts'); ?>
	</main>
<?php get_footer(); ?>