<?php 
	/*
		Template name: Оцените свой товар
	*/
	get_header(); 
?>
<main>
	<div class="rate">
		<div class="rate__inner">
			<h2 class="rate__header">ОЦЕНИТЕ СВОЙ ТОВАР</h2>
			<div class="rate__desc">Отправьте нам фотографии и характеристики товара, который вы хотели бы заложить, чтобы мы оценили его и связались с Вами.</div>
		</div>
		<div class="rate-form">
			<div class="rate-form__inner">
				<div class="rate-form__note">Поля помеченные звездочкой <span class="red-star">*</span> обязательны к заполнению.</div>
				<div class="rate-form__line">
					<div class="rate-form__desc rate-form__desc_cat">КАТЕГОРИЯ<span class="red-star">*</span></div>
					<div class="rate-form__control rate-form__control_cat">
						<select class="rate-form__select rate-form__select_cat require">
							<option value="" selected="selected">Выберите категорию</option>
							<option value="Ювелирные изделия">Ювелирные изделия</option>
							<option value="Сотовые телефоны">Сотовые телефоны</option>
							<option value="Ноутбуки">Ноутбуки</option>
							<option value="ЖК-телевизоры">ЖК-телевизоры</option>
							<option value="Цифровая техника">Цифровая техника</option>
							<option value="Меховые изделия">Меховые изделия</option>
							<option value="Разное">Разное</option>
						</select>
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Наименование товара<span class="red-star">*</span></div>
					<div class="rate-form__control">
						<input type="text" class="rate-form__input rate-form__input_goods require">
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Фотографии товара<span class="red-star">*</span></div>
					<div class="rate-form__control rate-form__control_photo">
						<label class="rate-form__label rate-form__label_file">
							<input type="file" class="rate-form__file rate-form__file_photo require">
							ВЫБЕРИТЕ ФАЙЛ
						</label>
						(jpg, png, gif)
						<span class="rate-form_file-name"></span>
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Описание товара</div>
					<div class="rate-form__control">
						<textarea class="rate-form__textarea rate-form__textarea_desc"></textarea>
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Оцените состояние товара<span class="red-star">*</span></div>
					<div class="rate-form__control rate-form__control_rate">
						<label class="rate-form__label rate-form__label_radio">
							<input type="radio" name="rate" class="rate-form__radio" value="Новый" checked="checked">
							Новый
						</label>
						<label class="rate-form__label rate-form__label_radio">
							<input type="radio" name="rate" class="rate-form__radio" value="Отличное">
							Отличное
						</label>
						<label class="rate-form__label rate-form__label_radio">
							<input type="radio" name="rate" class="rate-form__radio" value="Хорошее">
							Хорошее
						</label>
						<label class="rate-form__label rate-form__label_radio">
							<input type="radio" name="rate" class="rate-form__radio" value="Среднее">
							Среднее
						</label>
						<label class="rate-form__label rate-form__label_radio">
							<input type="radio" name="rate" class="rate-form__radio" value="Плохое">
							Плохое
						</label>
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Выезд к клиенту для оценки</div>
					<div class="rate-form__control rate-form__control_go-to">
						<label class="rate-form__label rate-form__label_radio">
							<input type="radio" value="Да" name="to-client">
							Да
						</label>
						<label class="rate-form__label rate-form__label_radio">
							<input type="radio" value="Нет" name="to-client" checked="checked">
							Нет
						</label>
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Ваше имя<span class="red-star">*</span></div>
					<div class="rate-form__control">
						<input type="text" class="rate-form__input rate-form__input_name require">
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Телефон<span class="red-star">*</span></div>
					<div class="rate-form__control">
						<input type="text" class="rate-form__input rate-form__input_phone require">
						<label>
							<input type="checkbox" class="rate-form__checkbox rate-form__checkbox_sms">
							<span class="rate-form__control-text">Я согласен(а) получать СМС-уведомления <br> о выгодных преlложениях от S-Ломбард</span>
						</label>
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Электронная почта</div>
					<div class="rate-form__control">
						<input type="input" class="rate-form__input rate-form__input_email">
						<label>
							<input type="checkbox" class="rate-form__checkbox rate-form__checkbox_mails">
							<span class="rate-form__control-text">Я согласен(а) получать уведомления <br> о выгодных преlложениях от S-Ломбард</span>
						</label>
					</div>
				</div>
				<div class="rate-form__line">
					<div class="rate-form__desc">Введите текст с картинки<span class="red-star">*</span></div>
					<div class="rate-form__control">
						<span class="rate-form__captcha"><img src="/wp-content/themes/slombard/captcha/captcha.php"></span>
						<input type="text" class="rate-form__input rate-form__input_captcha">
						<button class="rate-form__button">ОТПРАВИТЬ ЗАПРОС</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>